import React from "react";

export const PhotoContext = React.createContext({
    photo: null,
    setPhoto: () => null
});
