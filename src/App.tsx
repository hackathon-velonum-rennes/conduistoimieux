import React, {useState} from 'react';
import {Redirect, Route} from 'react-router-dom';
import {
    IonApp,
    IonIcon,
    IonLabel,
    IonRouterOutlet,
    IonTabBar,
    IonTabButton,
    IonTabs
} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';
import {apps, flash, send} from 'ionicons/icons';
import Tab2 from './pages/Tab2';
import Picture from "./pages/Tab1";


/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import {Photo} from "./useCamera";
import { PhotoContext } from './photo';

const App: React.FC = () => {
    const [photo, setPhoto] = useState<Photo>();

    return (
        <IonApp>
            <PhotoContext.Provider value={({photo, setPhoto} as any)}>
                <IonReactRouter>
                    <IonTabs>

                        <IonRouterOutlet>
                            <Route path="/home" component={Picture} exact={true} />
                            <Route path="/tab2" component={Tab2} exact={true} />
                            <Route path="/" render={() => <Redirect to="/home" />} exact={true} />
                        </IonRouterOutlet>

                        <IonTabBar slot="bottom" />
                    </IonTabs>
                </IonReactRouter>
            </PhotoContext.Provider>
        </IonApp>
    )
};

export default App;

/*
 <IonTabBar slot="bottom">
                    <IonTabButton tab="tab1" href="/home">
                        <IonIcon icon={flash} />
                        <IonLabel>Signaler</IonLabel>
                    </IonTabButton>
                    <IonTabButton tab="tab2" href="/tab2">
                        <IonIcon icon={apps} />
                        <IonLabel>Tab Two</IonLabel>
                    </IonTabButton>
                    <IonTabButton tab="tab3" href="/tab3">
                        <IonIcon icon={send} />
                        <IonLabel>Tab Three</IonLabel>
                    </IonTabButton>
                </IonTabBar>
 */
