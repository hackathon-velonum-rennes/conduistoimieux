import {useCamera} from "@ionic/react-hooks/camera";
import {CameraResultType, CameraSource} from "@capacitor/core";
import {useContext} from "react";
import {PhotoContext} from "./photo";

export interface Photo {
    filepath: string;
    webviewPath?: string;
    base64?: string;
}

export function usePhotoGallery() {
    const {photo, setPhoto} = useContext<any>(PhotoContext);
    const {getPhoto} = useCamera();

    const takePhoto = async () => {
        const cameraPhoto = await getPhoto({
            resultType: CameraResultType.Uri,
            source: CameraSource.Camera,
            quality: 100
        });
        const fileName = new Date().getTime() + '.jpeg';
        setPhoto({
            filepath: fileName,
            webviewPath: cameraPhoto.webPath
        })
    };

    return {
        photo,
        takePhoto,
    };
}

