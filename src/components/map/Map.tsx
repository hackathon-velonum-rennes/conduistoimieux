import React, {useRef, useState} from "react";
import {Map, Marker, Popup, TileLayer} from 'react-leaflet'

const position = {
    latlng: {
        lat:  48.8543,
        lng: 2.3527,
    },
};

const Mapper: React.FC<any> = () => {
    const [latlng, setlatlng] = useState(position.latlng);
    const map = useRef<any>();

    const handleLocationFound = (e: any) => {
        console.log({latlng: e.latlng});
        setlatlng(e.latlng);
    };

    return (
        <Map
            center={latlng}
            onLocationfound={handleLocationFound}
            zoom={16}
            zoomControl={false}
            attributionControl={false}
            ref={map}
        >
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={position.latlng}>
                <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
            </Marker>
        </Map>
    )
};

export default Mapper;
