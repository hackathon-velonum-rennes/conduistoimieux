import React, {useRef} from 'react';
import {
    IonCol,
    IonContent, IonFab,
    IonFabButton, IonGrid,
    IonHeader, IonIcon, IonImg,
    IonPage, IonRow,
    IonTitle,
    IonToolbar
} from '@ionic/react';
import {arrowUp, camera, car, flashlight} from 'ionicons/icons';
import {usePhotoGallery} from "../useCamera";
import img from './1.png'
import {Link} from "react-router-dom";

const Picture: React.FC = () => {
    const video = useRef<any>();
    const {takePhoto, photo} = usePhotoGallery();

    navigator.mediaDevices
        .getUserMedia({video: true})
        .then(stream => video.current.srcObject = stream)
        .catch(console.log);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Faire un signalement</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonGrid>
                    <IonRow>
                        <IonCol style={{overflow: "hidden", height: "30vh"}}>
                            {photo
                                ? (<IonImg src={photo.webviewPath} />)
                                : (<video ref={video}
                                          width="100%"
                                          autoPlay
                                          title="media stream" />)}
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol style={{overflow: "hidden", height: "30vh"}}>
                            <IonImg src={img} />
                        </IonCol>
                    </IonRow>

                </IonGrid>
                <IonFab vertical="bottom" horizontal="start" slot="fixed">
                    <IonFabButton onClick={() => takePhoto()}>
                        <IonIcon icon={camera} />
                    </IonFabButton>
                </IonFab>
                <IonFab vertical="bottom" horizontal="end">
                    <IonFabButton><Link to="/tab2"><IonIcon icon={car} size="large" color="light" /></Link></IonFabButton>
                    <IonFabButton><IonIcon icon={arrowUp} /></IonFabButton>
                    <IonFabButton><IonIcon icon={flashlight} /></IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default Picture;
/*
                 <IonRow>
                        <IonCol>
                            <IonButton size="large" color="light"><IonIcon icon={car} /></IonButton>
                            <IonButton size="large" color="light">Sas Vélo</IonButton>
                            <IonButton size="large" color="light">Doublement</IonButton>
                            <IonButton size="large" color="light"><IonIcon icon={arrowUp} /></IonButton>
                        </IonCol>
                    </IonRow>
 */
