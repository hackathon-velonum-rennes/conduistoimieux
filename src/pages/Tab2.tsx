import React from 'react';
import {
    IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle,
    IonContent, IonFab, IonFabButton,
    IonHeader, IonIcon, IonImg,
    IonPage,
    IonTitle,
    IonToolbar
} from '@ionic/react';
import {usePhotoGallery} from "../useCamera";
import {Link} from "react-router-dom";
import {car} from "ionicons/icons";

const Tab2: React.FC = () => {
    const {photo} = usePhotoGallery();

    console.log(photo)

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Validation</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonCard>
                    <IonCardHeader>
                        <IonImg src={photo ? photo.webviewPath : ''} />

                        <IonCardSubtitle>EE-311-WH - Renault Kangoo</IonCardSubtitle>
                        <IonCardTitle>Rue de Paris</IonCardTitle>
                    </IonCardHeader>

                    <IonCardContent>
                        Merci Pour votre signalement
                    </IonCardContent>
                </IonCard>
                <IonFab vertical="bottom" horizontal="end">
                    <IonFabButton><Link to="/home"><IonIcon icon={car} size="large" color="light"/></Link></IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default Tab2;
